<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::pattern('student_no', 's[0-9]{10}'); //統一格式定義

Route::group(['prefix'=>'student'], function(){
    
    // Route::get('{student_no}', function($student_no) {
    //     return "學號:".$student_no;
    // });
    Route::get('{student_no}', [
        'as' => 'student', //命名路由, as指定路由識別名稱
        'uses' => 'StudentController@getStudentData'
    ]);

    Route::get('{student_no}/score/{subject?}', [
        'as' => 'student.score', 
        'uses' => 'StudentController@getStudentScore'
    ])->where(['subject'=>'(chinese|english|math)']);
});